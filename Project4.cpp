﻿#include <iostream>

void FindOddNumbers(int Limit, bool IsOdd)
{    
    for (int i = 0; i <= Limit; i++)
    {
        if (IsOdd == true && i % 2 == 0)
        {
            std::cout << i << '\n';
        }
        if(IsOdd == false && i % 2 != 0)
        {
            std::cout << i << '\n';
        }        
    }
}

int main()
{
    setlocale(LC_ALL, "RU");

    int n = 6;
    std::cout << "Задайте количество цифр: " << "\n";
    std::cin >> n;
    
    FindOddNumbers(n, true);

    return 0;
}